export interface LoanSubmission {
  loanSubmissionId: number,
  status: LoanAcceptanceStatusOption,
  title: string,
  fullName: string,
  street: string,
  city: string,
  county: string,
  postCode: string,
  mobileNumber: string,
  emailAddress: string,
  annualIncome: number,
  isHomeOwner: boolean,
  carRegistration: string,
  loanRequestAmount: number,
  totalPayable: number,
  numberOfTerms: number,
  repaymentPerMonth: number,
  interestRate: number,
  apr: number
}

export interface LoanResponse {
  loanSubmissionDto: LoanSubmission;
}

export interface LoanAcceptanceRequest{
  loanSubmissionId:number,
  statusOption:LoanAcceptanceStatusOption
}

export interface LoanAcceptanceResponse{
  wasChangeSuccessful: boolean
}

export enum LoanAcceptanceStatusOption {
  InProgress,
  Approved,
  Denied
}

export interface GetLoansListRequest {
  fullName: string
}

export interface GetLoansListResponse {
  loanSubmissions: LoanSubmission[];
}
