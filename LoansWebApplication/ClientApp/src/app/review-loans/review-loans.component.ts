import {Component, OnInit} from '@angular/core';
import {FormBuilder} from '@angular/forms';
import {Validators} from '@angular/forms';
import {FormArray} from '@angular/forms';
import {LoanAcceptanceRequest, LoanAcceptanceStatusOption, LoanSubmission} from '../models/models';
import {LoanService} from '../services/loan.service';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-review-loans',
  templateUrl: './review-loans.component.html',
  styleUrls: ['./review-loans.component.css']
})
export class ReviewLoansComponent implements OnInit {

  public loanSubmissions: LoanSubmission[];

  public isSnackBarVisible: boolean;
  private snackBarTimer: number;

  constructor(private loanService: LoanService) {
  }

  ngOnInit() {
    this.loanService.getLoans({fullName: null})
      .subscribe(
        (resp) => {
          console.log(resp);
          this.loanSubmissions = resp.loanSubmissions;
          console.log(this.loanSubmissions);
        });
  }

  approveSelectionChanged(event: Event, id: number) {
    let selectedVal: number = +((event.target as HTMLSelectElement).value);
    let statusOption: LoanAcceptanceStatusOption = selectedVal;
    let request: LoanAcceptanceRequest = {loanSubmissionId: id, statusOption: statusOption};
    this.loanService.changeLoanAcceptanceStatus(request).subscribe(x => {
      console.log(x);
      if (x.wasChangeSuccessful) {
        this.showSnackBar();
      }
    });
  }

  showSnackBar() {
    if (this.snackBarTimer) {
      clearTimeout(this.snackBarTimer);
    }
    this.isSnackBarVisible = true;
    this.snackBarTimer = setTimeout(() => {
      this.isSnackBarVisible = false;
    }, 5000);
  }
}
