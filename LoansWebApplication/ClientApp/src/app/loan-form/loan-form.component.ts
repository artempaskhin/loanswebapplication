import {Component, OnInit} from '@angular/core';
import {FormBuilder} from '@angular/forms';
import {Validators} from '@angular/forms';
import {FormArray} from '@angular/forms';
import {LoanSubmission} from '../models/models';
import {LoanService} from '../services/loan.service';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-loan-form',
  templateUrl: './loan-form.component.html',
  styleUrls: ['./loan-form.component.css']
})
export class LoanFormComponent implements OnInit {

  carRegistrationRegex = "(^[A-Z]{2}[0-9]{2}\\s?[A-Z]{3}$)|(^[A-Z][0-9]{1,3}[A-Z]{3}$)|(^[A-Z]{3}[0-9]{1,3}[A-Z]$)|(^[0-9]{1,4}[A-Z]{1,2}$)|(^[0-9]{1,3}[A-Z]{1,3}$)|(^[A-Z]{1,2}[0-9]{1,4}$)|(^[A-Z]{1,3}[0-9]{1,3}$)|(^[A-Z]{1,3}[0-9]{1,4}$)|(^[0-9]{3}[DX]{1}[0-9]{3}$)";
  postCodeRegex= /^(GIR[ ]?0AA|((AB|AL|B|BA|BB|BD|BH|BL|BN|BR|BS|BT|CA|CB|CF|CH|CM|CO|CR|CT|CV|CW|DA|DD|DE|DG|DH|DL|DN|DT|DY|E|EC|EH|EN|EX|FK|FY|G|GL|GY|GU|HA|HD|HG|HP|HR|HS|HU|HX|IG|IM|IP|IV|JE|KA|KT|KW|KY|L|LA|LD|LE|LL|LN|LS|LU|M|ME|MK|ML|N|NE|NG|NN|NP|NR|NW|OL|OX|PA|PE|PH|PL|PO|PR|RG|RH|RM|S|SA|SE|SG|SK|SL|SM|SN|SO|SP|SR|SS|ST|SW|SY|TA|TD|TF|TN|TQ|TR|TS|TW|UB|W|WA|WC|WD|WF|WN|WR|WS|WV|YO|ZE)(\d[\dA-Z]?[ ]?\d[ABD-HJLN-UW-Z]{2}))|BFPO[ ]?\d{1,4})$/;
  profileForm = this.fb.group({
    title: ['Mr', Validators.required],
    fullName: ['', Validators.required],
    street: ['', Validators.required],
    city: ['', Validators.required],
    county: [''],
    postCode: ['', Validators.compose([Validators.pattern(this.postCodeRegex),Validators.required])],
    mobileNumber: ['', Validators.required],
    emailAddress: ['', Validators.compose([Validators.email,Validators.required])],
    annualIncome: ['', Validators.compose([Validators.min(0),Validators.required])],
    isHomeOwner: [false, Validators.required],
    carRegistration: ['',Validators.pattern(this.carRegistrationRegex)],
    loanRequestAmount: ['', Validators.compose([Validators.min(0),Validators.required])],
  });


  constructor(private fb: FormBuilder, private loanService: LoanService) {
  }

  loanSubmissionResponse: LoanSubmission;

  onSubmit() {
    // TODO: Use EventEmitter with form value
    let loanSubmission: LoanSubmission = this.profileForm.value;
    console.log(loanSubmission);
    this.loanService.submitLoan(loanSubmission).subscribe(resp => {
        //TODO
        this.loanSubmissionResponse= resp.loanSubmissionDto;
        console.log(this.loanSubmissionResponse);
        this.profileForm.disable();
      },
      (error => {
        console.warn('Error' + error);
      }));
  }

  ngOnInit() {
  }

}
