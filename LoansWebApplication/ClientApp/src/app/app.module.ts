import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {RouterModule} from '@angular/router';

import {AppComponent} from './app.component';
import {NavMenuComponent} from './nav-menu/nav-menu.component';
import {ReviewLoansComponent} from './review-loans/review-loans.component';
import {LoanFormComponent} from './loan-form/loan-form.component';
import {ViewStatusComponent} from './view-status/view-status.component';

@NgModule({
  declarations: [
    AppComponent,
    NavMenuComponent,
    ReviewLoansComponent,
    LoanFormComponent,
    ViewStatusComponent
  ],
  imports: [
    BrowserModule.withServerTransition({appId: 'ng-cli-universal'}),
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot([
      {path: '', component: LoanFormComponent, pathMatch: 'full'},
      {path: 'status', component: ViewStatusComponent},
      {path: 'review', component: ReviewLoansComponent},
    ])
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
