import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {HttpErrorResponse, HttpResponse} from '@angular/common/http';

import {Observable, throwError} from 'rxjs';
import {catchError, retry} from 'rxjs/operators';
import {
  GetLoansListRequest,
  GetLoansListResponse,
  LoanAcceptanceRequest,
  LoanAcceptanceResponse,
  LoanResponse,
  LoanSubmission
} from '../models/models';

interface Config {
}


const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
};

@Injectable({
  providedIn: 'root'
})
export class LoanService {

  configUrl = 'assets/config.json';
  submitLoansUrl = 'api/loans/submitLoan';
  getLoansUrl = 'api/loans/getLoans';
  changeLoanAcceptanceStatusUrl = 'api/loans/changeAccept';

  constructor(private http: HttpClient) {
  }

  submitLoan(loanSub: LoanSubmission): Observable<LoanResponse> {
    return this.http.post<LoanResponse>(this.submitLoansUrl, loanSub, httpOptions)
      .pipe(
        catchError(this.handleError)
      );
  }

  getLoans(req:GetLoansListRequest):Observable<GetLoansListResponse>{
    return this.http.post<GetLoansListResponse>(this.getLoansUrl, req, httpOptions)
      .pipe(
        catchError(this.handleError)
      );
  }

  changeLoanAcceptanceStatus(req:LoanAcceptanceRequest):Observable<LoanAcceptanceResponse>{
    return this.http.post<LoanAcceptanceResponse>(this.changeLoanAcceptanceStatusUrl, req, httpOptions)
      .pipe(
        catchError(this.handleError)
      );
  }

  private handleError(error: HttpErrorResponse) {
    if (error.status === 0) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong.
      console.error(
        `Backend returned code ${error.status}, body was: `, error.error);
    }
    // Return an observable with a user-facing error message.
    return throwError(
      'Something bad happened; please try again later.');
  }

  makeIntentionalError() {
    return this.http.get('not/a/real/url')
      .pipe(
        catchError(this.handleError)
      );
  }

}
