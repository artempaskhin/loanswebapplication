import {Component, OnInit} from '@angular/core';
import {FormBuilder, Validators} from '@angular/forms';
import {LoanService} from '../services/loan.service';
import {GetLoansListRequest, LoanAcceptanceRequest, LoanAcceptanceStatusOption, LoanSubmission} from '../models/models';

@Component({
  selector: 'app-view-status',
  templateUrl: './view-status.component.html',
  styleUrls: ['./view-status.component.css']
})
export class ViewStatusComponent implements OnInit {

  friendlyNames: any = {
    0: 'In Progress',
    1: 'Approved',
    2: 'Denied',
  };

  searchForm = this.fb.group({
    emailAddress: ['', Validators.email],
  });

  hasSearched:boolean;
  loanSubmissions: LoanSubmission[];

  constructor(private fb: FormBuilder, private loanService: LoanService) {
  }

  ngOnInit() {
  }

  onSubmit() {
    // TODO: Use EventEmitter with form value
    let getLoansListRequest: GetLoansListRequest = this.searchForm.value;
    console.log(getLoansListRequest);
    this.loanService.getLoans(getLoansListRequest).subscribe(resp => {
        //TODO
        this.hasSearched = true;
        this.loanSubmissions = resp.loanSubmissions;
        console.log(this.loanSubmissions);
      },
      (error => {
        console.warn('Error' + error);
      }));
  }

}
