﻿using AutoMapper;
using LoansWebApplication.Handlers;
using LoansWebApplication.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LoansWebApplication.AutoMapper
{
    public static class AutoMapperConfig
    {
        public static MapperConfiguration RegisterMappings()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateLoanSubmissionMappings();
            });

            config.AssertConfigurationIsValid();

            return config;
        }
        public static IMapperConfigurationExpression CreateLoanSubmissionMappings(this IMapperConfigurationExpression cfg)
        {
            cfg.CreateMap<LoanSubmissionDto, LoanSubmission>(MemberList.Source);
            cfg.CreateMap<LoanSubmission, LoanSubmissionDto>(MemberList.Destination);
            return cfg;
        }

    }
}
