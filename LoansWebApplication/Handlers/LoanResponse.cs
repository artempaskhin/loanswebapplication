﻿using LoansWebApplication.Models;

namespace LoansWebApplication.Handlers
{
    public class LoanResponse
    {
        public LoanSubmissionDto LoanSubmissionDto { get; set; }
    }
}
