﻿using MediatR;

namespace LoansWebApplication.Handlers
{
    public class GetLoansListRequest : IRequest<GetLoansListResponse>
    {
        public string EmailAddress { get; set; }
    }

}
