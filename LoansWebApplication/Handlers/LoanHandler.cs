﻿using AutoMapper;
using LoansWebApplication.Models;
using LoansWebApplication.Utilities;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LoansWebApplication.Handlers
{
    public class LoanHandler : IRequestHandler<LoanRequest, LoanResponse>, IRequestHandler<LoanAcceptanceRequest, LoanAcceptanceResponse>, IRequestHandler<GetLoansListRequest, GetLoansListResponse>

    {
        private readonly ILogger<LoanHandler> _logger;
        private readonly LoansDbContext _context;
        private readonly ILoanCalculator _calculator;
        private readonly IMapper _mapper;

        public LoanHandler(ILogger<LoanHandler> logger, LoansDbContext context, ILoanCalculator calculator, IMapper mapper)
        {
            _logger = logger;
            _context = context;
            _calculator = calculator;
            _mapper = mapper;
        }

        public async Task<LoanResponse> Handle(LoanRequest request, CancellationToken cancellationToken)
        {
            LoanSubmission submission = request.LoanSubmission;
            LoanInformation info = _calculator.Calculate(submission);

            submission.TotalPayable = info.TotalPayable;
            submission.NumberOfTerms = info.NumberOfTerms;
            submission.RepaymentPerMonth = info.RepaymentPerMonth;
            submission.InterestRate = info.InterestRate;
            submission.APR = info.APR;

            _context.LoansSubmissions.Add(submission);
            await _context.SaveChangesAsync();

            LoanSubmissionDto dto = _mapper.Map<LoanSubmissionDto>(submission);
            LoanResponse resp = new LoanResponse()
            {
                LoanSubmissionDto = dto
            };

            return resp;
        }

        public async Task<LoanAcceptanceResponse> Handle(LoanAcceptanceRequest request, CancellationToken cancellationToken)
        {
            LoanAcceptanceResponse response;
            LoanSubmission submission = _context.LoansSubmissions.FirstOrDefault(x => x.LoanSubmissionId == request.LoanSubmissionId);

            if (submission == null)
            {
                response = new LoanAcceptanceResponse() { WasChangeSuccessful = false };
                return response;
            }

            submission.Status = request.StatusOption;
            _context.LoansSubmissions.Update(submission);
            await _context.SaveChangesAsync();

            response = new LoanAcceptanceResponse() { WasChangeSuccessful = true };
            return response;
        }

        public async Task<GetLoansListResponse> Handle(GetLoansListRequest request, CancellationToken cancellationToken)
        {
            List<LoanSubmission> submissions = null;
            if (request.EmailAddress == null)
            {
                submissions = await _context.LoansSubmissions.ToListAsync();
            }
            else
            {
                submissions = await _context.LoansSubmissions.Where(x => x.EmailAddress == request.EmailAddress).ToListAsync();
            }

            GetLoansListResponse resp = new GetLoansListResponse()
            {
                LoanSubmissions = submissions
            };
            return resp;
        }
    }
}
