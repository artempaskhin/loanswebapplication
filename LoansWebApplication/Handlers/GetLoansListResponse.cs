﻿using LoansWebApplication.Models;
using System.Collections.Generic;

namespace LoansWebApplication.Handlers
{
    public class GetLoansListResponse
    {
        public List<LoanSubmission> LoanSubmissions { get; set; }
    }

}
