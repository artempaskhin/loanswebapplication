﻿using LoansWebApplication.Models;
using MediatR;

namespace LoansWebApplication.Handlers
{
    public class LoanAcceptanceRequest : IRequest<LoanAcceptanceResponse>
    {
        public int LoanSubmissionId { get; set; }
        public LoanAcceptanceStatusOption StatusOption { get; set; }
    }
}
