﻿using LoansWebApplication.Models;
using MediatR;
using System.Collections.Generic;
using System.Linq;

namespace LoansWebApplication.Handlers
{
    public class LoanRequest : IRequest<LoanResponse>
    {
        public LoanSubmission LoanSubmission { get; set; }
    }
}
