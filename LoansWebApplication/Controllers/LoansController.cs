﻿using AutoMapper;
using LoansWebApplication.Handlers;
using LoansWebApplication.Models;
using LoansWebApplication.Utilities;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace LoansWebApplication.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LoansController : ControllerBase
    {
        private readonly IMediator _mediator;
        private readonly ILogger<LoansController> _logger;
        private readonly IMapper _mapper;

        public LoansController(IMediator mediator, ILogger<LoansController> logger, IMapper mapper)
        {
            _mediator = mediator;
            _logger = logger;
            _mapper = mapper;
        }

        // POST api/<RequestLoanController>
        [HttpPost("submitLoan")]
        public async Task<JsonResult> SubmitLoan([FromBody] LoanSubmissionDto submissionDto)
        {
            LoanSubmission submission = _mapper.Map<LoanSubmission>(submissionDto);
            LoanRequest request = new LoanRequest
            {
                LoanSubmission = submission
            };
            LoanResponse response = await _mediator.Send(request);
            return new JsonResult(response);
        }

        [HttpPost("getLoans")]
        public async Task<JsonResult> GetLoans([FromBody] GetLoansListRequest request)
        {
            GetLoansListResponse response = await _mediator.Send(request);
            return new JsonResult(response);
        }

        [HttpPost("changeAccept")]
        public async Task<JsonResult> ChangeLoanAcceptanceStatus([FromBody] LoanAcceptanceRequest request)
        {
            LoanAcceptanceResponse response = await _mediator.Send(request);
            return new JsonResult(response);
        }
    }
}
