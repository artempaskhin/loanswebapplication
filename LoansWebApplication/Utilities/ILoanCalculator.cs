﻿using LoansWebApplication.Models;

namespace LoansWebApplication.Utilities
{
    public interface ILoanCalculator
    {
        LoanInformation Calculate(LoanSubmission submission);
    }
}