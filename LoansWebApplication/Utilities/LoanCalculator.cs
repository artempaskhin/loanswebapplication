﻿using LoansWebApplication.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LoansWebApplication.Utilities
{
    public class LoanCalculator : ILoanCalculator
    {
        public LoanInformation Calculate(LoanSubmission submission)
        {
            double interestRate = 0.173;
            int numMonths = 18;
            double loanAmount = submission.LoanRequestAmount;

            //double monthlyPayment = loanAmount / (((1 - (1 / Math.Pow(1 + interestRate, numMonths)) / interestRate)));
            //double totalPayable = 18 * monthlyPayment;

            var ln = Math.Pow(1 + interestRate, numMonths);
            var monthlyPayment = loanAmount / ((1 - (1 / ln)) / interestRate);
            var totalPayable = monthlyPayment * numMonths;
            var apr = 12 * interestRate;

            LoanInformation result = new LoanInformation();
            result.IsSuccessful = true;
            result.LoanAmount = submission.LoanRequestAmount;
            result.NumberOfTerms = numMonths;
            result.InterestRate = interestRate;
            result.RepaymentPerMonth = monthlyPayment;
            result.TotalPayable = totalPayable;
            result.APR = apr;
            return result;
        }
    }
}
