﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoansWebApplication.Models
{
    public class LoanInformation
    {
        public bool IsSuccessful { get; set; }
        public double LoanAmount { get; set; }
        public double TotalPayable { get; set; }
        public int NumberOfTerms { get; set; }
        public double RepaymentPerMonth { get; set; }
        public double InterestRate { get; set; }
        public double APR { get; set; }
    }
}
