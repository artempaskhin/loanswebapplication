﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoansWebApplication.Models
{
    public class LoansDbContext : DbContext
    {
        public LoansDbContext(DbContextOptions options) : base(options)
        {

        }

        public virtual DbSet<LoanSubmission> LoansSubmissions { get; set; }
    }
}
