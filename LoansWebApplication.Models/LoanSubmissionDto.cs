﻿namespace LoansWebApplication.Models
{
    public class LoanSubmissionDto
    {
        public string Title { get; set; }
        public string FullName { get; set; }
        public string Street { get; set; }
        public string City { get; set; }
        public string County { get; set; }
        public string PostCode { get; set; }
        public string MobileNumber { get; set; }
        public string EmailAddress { get; set; }
        public int AnnualIncome { get; set; }
        public bool IsHomeOwner { get; set; }
        public string CarRegistration { get; set; }
        public double LoanRequestAmount { get; set; }

        public double TotalPayable { get; set; }
        public int NumberOfTerms { get; set; }
        public double RepaymentPerMonth { get; set; }
        public double InterestRate { get; set; }
        public double APR { get; set; }
    }
}
